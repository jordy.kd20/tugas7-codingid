﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tugas7codingid.Logics;
using Tugas7codingid.Models;

namespace Tugas7codingid.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SiswaController : ControllerBase
    {
        [HttpPost]
        [Route("tasks/AddUserWithTask")]
        public ActionResult InsertNameTask([FromBody] addUserTask siswa)
        {
            try
            {
                Users user = new Users
                {
                    name = siswa.name
                };

                List<Tasks> tasks = new List<Tasks>();
                foreach (var item in siswa.task)
                {
                    tasks.Add(new Tasks
                    {
                        task_detail = item.task_detail
                    });
                }

               CRUD.CreateUserAccount(user, tasks);
               return StatusCode(201, "Berhasil");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("tasks/GetUserWithTask")]
        public ActionResult Getalldatasiswa()
        {
            try
            {
                List<Users> userall = CRUD.GetallUser();
                List<Tasks> task = CRUD.GetTasks();
                List<GetSiswa> getSiswas = new List<GetSiswa>();


               
                foreach (var item in userall)
                {
                    List<TaskUser> temptask = new List<TaskUser>();
                    foreach (var nilai in task)
                    {
                        if(item.pk_user_id == nilai.fk_users_id)
                        {
                            temptask.Add(new TaskUser
                            {
                                pk_task_id = nilai.pk_task_id,
                                task_detail = nilai.task_detail
                            });
                        }
                    }
                    getSiswas.Add(new GetSiswa
                    {
                        tasks = temptask,
                        users = item
                    });


                }

                return Ok(getSiswas);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet]
        [Route("tasks/GetUserWithName")]
        public ActionResult GetdataName(string nama)
        {
            try
            {
                List<Users> userall = CRUD.GetallUser();
                List<Tasks> task = CRUD.GetTasks();
                List<GetSiswa> getSiswas = new List<GetSiswa>();



                foreach (var item in userall)
                {
                    List<TaskUser> temptask = new List<TaskUser>();
                    foreach (var nilai in task)
                    {
                        if (item.pk_user_id == nilai.fk_users_id)
                        {
                            temptask.Add(new TaskUser
                            {
                                pk_task_id = nilai.pk_task_id,
                                task_detail = nilai.task_detail
                            });
                        }
                    }
                    if(item.name == nama.ToString())
                    {
                        getSiswas.Add(new GetSiswa
                        {
                            tasks = temptask,
                            users = item
                        });
                    }
                    
                }

                return Ok(getSiswas);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
