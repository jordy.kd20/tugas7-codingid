﻿using System.Data;
using System.Data.SqlClient;

namespace Tugas7codingid.Models
{
    public class CRUD
    {
        private static string connectionString = "Server=LAPTOP-GKGN6QTN\\JORDYKD;Database=siswa;Trusted_Connection=True;";
        

        #region insert Siswa 
        public static void CreateUserAccount(Users user, List<Tasks> task)
        {
            int lastValued = 0;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "INSERT into Users (name) VALUES (@name); select SCOPE_IDENTITY() AS [lastValue];";

                    cmd.Parameters.AddWithValue("@name", user.name);
                    lastValued = Convert.ToInt32(cmd.ExecuteScalar());
                   
                }

                foreach (var item in task)
                {
                    using (SqlCommand cmd2 = con.CreateCommand())
                    {
                        cmd2.Connection = con;
                        cmd2.CommandText = "INSERT into Tasks (task_detail,fk_users_id) VALUES (@task_detail, @fk_users_id);";
                        cmd2.Parameters.AddWithValue("@task_detail", item.task_detail);
                        cmd2.Parameters.AddWithValue("@fk_users_id", lastValued);
                        cmd2.ExecuteNonQuery();                    }
                }
                con.Close();
               
            }
            return ;
        }
        #endregion

        #region ambil all tasks
        public static List<Tasks> GetTasks()
        {
            List<Tasks> result = new List<Tasks>();
            using (SqlConnection con = new SqlConnection(connectionString)) 
            {
                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "Select * from Tasks";
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result.Add(new Tasks
                        {
                            pk_task_id = Convert.ToInt32(reader["pk_task_id"]),
                            task_detail = reader["task_detail"].ToString(),
                            fk_users_id = Convert.ToInt32(reader["fk_users_id"])
                        });
                    }
                   con.Close() ;
                   
                }
                return result ;
            }
            
        }
        #endregion

        #region ambil all user
        public static List<Users> GetallUser()
        {
            List<Users> resultuser = new List<Users>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "Select * from Users";
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        resultuser.Add(new Users
                        {
                            pk_user_id = Convert.ToInt32(reader["pk_user_id"]),
                            name = reader["name"].ToString() ?? string.Empty
                        });
                    }
                    con.Close();

                }
                return resultuser;
            }

        }
        #endregion



        public static DataTable ExecuteQuery(string sql)
        {
            DataTable result = new DataTable();

            #region query proccess database
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                #region query
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(result);
                }
                conn.Close();
                #endregion
            }
            #endregion

            return result;
        }

        #region ExecuteScalar
        /// <summary>
        /// ExecuteScalar untuk menjalankan query yang hanya return tepat 1 data
        /// </summary>
        public static object ExecuteScalar(string query)
        {
            object result = null;

            // begin connection
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                #region query process to database
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    result = cmd.ExecuteScalar(); // ExecuteScalar untuk query yang return tepat 1 data saja
                }
                #endregion

                // close connection
                con.Close();
            }

            return result;
        }
        #endregion

        #region ExecuteNonQuery
        /// <summary>
        /// ExecuteNonQuery untuk menjalankan query yang tidak return apa-apa
        /// </summary>
        public static int ExecuteNonQuery(string query)
        {
            int result = 0;

            // begin connection
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                #region query process to database
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    result = cmd.ExecuteNonQuery(); // ExecuteNonQuery untuk query yang tidak return apa-apa
                }
                #endregion

                // close connection
                con.Close();
            }

            return result;
        }
        #endregion


        
    }
}
